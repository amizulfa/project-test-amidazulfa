import "bootstrap/dist/css/bootstrap.css"
import "./globals.css"
import Bootstrap from "@/utils/bootstrap"
import { Montserrat } from "next/font/google"
import Navbar from "@/components/navbar"
import Banner from "@/components/banner"

const montserrat = Montserrat({
	subsets: ["latin"],
	weight: ["300", "400", "600", "700"],
})



export default function RootLayout({ children }) {
	return (
		<html lang="id">
			<body className={montserrat.className}>
				<Navbar />
				<Banner />
				{children}
				<Bootstrap />
			</body>
		</html>
	)
}
