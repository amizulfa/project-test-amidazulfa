"use client";

import "./ideas.css";
import { useState, useEffect } from "react";
import Image from "next/image";
import axios from "axios";

const ITEMS_PER_PAGE_OPTIONS = [10, 20, 50];
const SORT_BY_OPTIONS = ["Newest", "Oldest"];

const formatDateToIndonesian = (dateString) => {
  const date = new Date(dateString);
  const options = { day: "numeric", month: "long", year: "numeric" };
  return date.toLocaleDateString("id-ID", options);
};

export default function Ideas() {
  const [ideas, setIdeas] = useState([]);
  const [loading, setLoading] = useState(false);
  const [hasMore, setHasMore] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState(10);
  const [sortOrder, setSortOrder] = useState("-published_at");
  const [totalItems, setTotalItems] = useState(0);

  useEffect(() => {
    const fetchIdeas = async () => {
      setLoading(true);
      try {
        const response = await axios.get(
          `https://suitmedia-backend.suitdev.com/api/ideas?page[number]=${currentPage}&page[size]=${itemsPerPage}&append[]=small_image&append[]=medium_image&sort=${sortOrder}`
        );
        setIdeas(response.data.data);
        setTotalItems(response.data.meta ? response.data.meta.total : 0);
        setHasMore(response.data.meta.pagination.pages > currentPage);
      } catch (error) {
        console.error(error);
      } finally {
        setLoading(false);
      }
    };
    fetchIdeas();
  }, [currentPage, itemsPerPage, sortOrder]);

  const handleItemsPerPageChange = (event) => {
    setItemsPerPage(Number(event.target.value));
    setCurrentPage(1); 
  };

  const handleSortByChange = (event) => {
    if (event.target.value === "Newest") {
      setSortOrder("-published_at");
    } else {
      setSortOrder("published_at");
    }
    setCurrentPage(1); 
  };

  const handleFirstPage = () => {
    setCurrentPage(1);
  };

  const handlePreviousPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  const handleNextPage = () => {
    if (currentPage < totalPages) {
      setCurrentPage(currentPage + 1);
    }
  };

  const handleLastPage = () => {
    setCurrentPage(totalPages);
  };

  const totalPages = Math.ceil(totalItems / itemsPerPage);

  const renderPageNumbers = () => {
    const maxPagesToShow = 5;
    const pages = [];
    const startPage = Math.max(1, currentPage - Math.floor(maxPagesToShow / 2));
    const endPage = Math.min(totalPages, startPage + maxPagesToShow - 1);

    for (let page = startPage; page <= endPage; page++) {
      pages.push(
        <button
          key={page}
          className={`page-link ${currentPage === page ? "active" : ""}`}
          onClick={() => setCurrentPage(page)}
        >
          {page}
        </button>
      );
    }

    return pages;
  };

  return (
    <main id="ideas" className="container">
      <div className="d-flex justify-content-between align-items-center mb-4">
        <div>
          <span>
            Showing {((currentPage - 1) * itemsPerPage) + 1} -{" "}
            {Math.min(currentPage * itemsPerPage, totalItems)} of{" "}
            {totalItems}
          </span>
        </div>
        <div className="d-flex align-items-center">
          <div className="me-3">
            <label htmlFor="itemsPerPage" className="me-2">
              Show per page:
            </label>
            <select
              id="itemsPerPage"
              value={itemsPerPage}
              onChange={handleItemsPerPageChange}
              className="form-select d-inline-block w-auto"
            >
              {ITEMS_PER_PAGE_OPTIONS.map((option) => (
                <option key={option} value={option}>
                  {option}
                </option>
              ))}
            </select>
          </div>
          <div>
            <label htmlFor="sortBy" className="me-2">
              Sort by:
            </label>
            <select
              id="sortBy"
              value={sortOrder === "-published_at" ? "Newest" : "Oldest"}
              onChange={handleSortByChange}
              className="form-select d-inline-block w-auto"
            >
              {SORT_BY_OPTIONS.map((option) => (
                <option key={option} value={option}>
                  {option}
                </option>
              ))}
            </select>
          </div>
        </div>
      </div>
      <div className="row">
        {ideas.map((item) => (
          <div key={item.id} className="col-md-3 mb-4">
            <div className="card h-100">
              <Image
                src={item.small_image}
                alt={item.title}
                className="card-img-top"
                width={300}
                height={200}
                style={{ objectFit: "cover" }}
                loading="lazy"
              />
              <div className="card-body">
                <p className="card-text">
                  <small className="text-muted">
                    {formatDateToIndonesian(item.published_at)}
                  </small>
                </p>
                <h6
                  className="card-title"
                  style={{
                    display: "-webkit-box",
                    WebkitBoxOrient: "vertical",
                    WebkitLineClamp: 3,
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                    lineHeight: "1.2rem",
                    maxHeight: "3.6rem",
                  }}
                >
                  {item.title}
                </h6>
              </div>
            </div>
          </div>
        ))}
      </div>
      <div className="d-flex justify-content-center">
        <div className="pagination">
          <button
            className="page-link"
            onClick={handleFirstPage}
            disabled={currentPage === 1}
          >
            &laquo;&laquo;
          </button>
          <button
            className="page-link"
            onClick={handlePreviousPage}
            disabled={currentPage === 1}
          >
            &laquo;
          </button>
          {renderPageNumbers()}
          <button
            className="page-link"
            onClick={handleNextPage}
            disabled={currentPage === totalPages}
          >
            &raquo;
          </button>
          <button
            className="page-link"
            onClick={handleLastPage}
            disabled={currentPage === totalPages}
          >
            &raquo;&raquo;
          </button>
        </div>
      </div>
    </main>
  );
}
