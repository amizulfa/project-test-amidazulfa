"use client"
import Link from "next/link"
import { usePathname } from "next/navigation"
import { useState, useEffect } from "react"

export default function Navbar() {
  const route = usePathname()
  const [show, setShow] = useState(true)
  const [lastScrollY, setLastScrollY] = useState(0)
  const [atTop, setAtTop] = useState(true)

  const controlNavbar = () => {
    if (typeof window !== 'undefined') {
      if (window.scrollY > lastScrollY) {
        // Scroll down
        setShow(false)
      } else {
        // Scroll up
        setShow(true)
      }
      setLastScrollY(window.scrollY)
      setAtTop(window.scrollY === 0)
    }
  }

  useEffect(() => {
    if (typeof window !== 'undefined') {
      window.addEventListener('scroll', controlNavbar)
      return () => {
        window.removeEventListener('scroll', controlNavbar)
      }
    }
  }, [lastScrollY])

  return (
    <nav className={`navbar navbar-expand-lg fixed-top pt-3 pb-3 ${show ? 'navbar-show' : 'navbar-hide'} ${atTop ? 'at-top' : ''}`}>
      <div className="container">
        <img
          src="/assets/logo.png"
          alt="Logo Suitmedia"
          style={{
            height: "50px",
            width: "100px",
          }}
        />
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse gap-4" id="navbarNav">
          <ul className="navbar-nav ms-auto align-items-center gap-2">
            <li className="nav-item">
              <Link
                className={`nav-link ${route === "/work" ? "active" : ""}`}
                href={"/work"}>
                Work
              </Link>
            </li>
            <li className="nav-item">
              <Link
                className={`nav-link ${route === "/about" ? "active" : ""}`}
                href={"/about"}>
                About
              </Link>
            </li>
            <li className="nav-item">
              <Link
                className={`nav-link ${route === "/service" ? "active" : ""}`}
                href={"/service"}>
                Service
              </Link>
            </li>
            <li className="nav-item">
              <Link
                className={`nav-link ${route === "/ideas" ? "active" : ""}`}
                href={"/ideas"}>
                Ideas
              </Link>
            </li>
            <li className="nav-item">
              <Link
                className={`nav-link ${route === "/carriers" ? "active" : ""}`}
                href={"/carriers"}>
                Carriers
              </Link>
            </li>
            <li className="nav-item">
              <Link
                className={`nav-link ${route === "/contact" ? "active" : ""}`}
                href={"/contact"}>
                Contact
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}
