"use client";

import Image from "next/image";
import { useEffect } from "react";

export default function Banner() {
	const handleScroll = () => {
		const scrollTop = window.pageYOffset;
		const bannerImage = document.querySelector('.banner-image');
		const bannerContent = document.querySelector('.banner-content');
		
		bannerImage.style.transform = `translateY(${scrollTop * 0.5}px)`;
		bannerContent.style.transform = `translate(-50%, calc(-50% + ${scrollTop * 0.2}px))`;
	};

	useEffect(() => {
		window.addEventListener('scroll', handleScroll);
		return () => {
			window.removeEventListener('scroll', handleScroll);
		};
	}, []);

	return (
		<section className="banner">
			<div className="banner-content">
				<p style={{ fontSize: '2.5rem' }}>Ideas</p>
				<p>Where all our great things begin</p>
			</div>
			<div className="banner-image">
				<Image 
					src="/assets/banner.jpg" 
					alt="Banner Image" 
					fill 
					style={{ objectFit: 'cover' }}
				/>
			</div>
		</section>
	)
}
