module.exports = {
    //...
    async rewrites() {
      return [
        {
          source: "/api/ideas",
          destination: "https://suitmedia-backend.suitdev.com/api/ideas",
        },
      ];
    },
  };